# coding=utf-8
from django.db.models import Q
from django.shortcuts import render, redirect
from django.http import HttpResponse

from django.core.urlresolvers import reverse
from core.models import Story, StoryPart, Lock
from core.forms import StoryPartForm


def check_stories():
    stories_count = Story.objects.filter(finished=False).count()
    if stories_count < 10:
        stories = []
        for x in xrange(0, 10 - stories_count):
            stories.append(Story(finished=False))
        Story.objects.bulk_create(stories)


def index(request):
    check_stories()

    # todo: не давать игроку писать два раза подряд
    #: :type : django.contrib.authentication.models.User
    user = request.user

    if user.is_authenticated():
        if Lock.objects.filter(user=user).exists():
            last_part = user.lock.part
            story = last_part.story
            prev_part = last_part.prev
        else:
            good_parts = StoryPart.objects.filter(next__isnull=True, lock__isnull=True).exclude(prev__user=user)
            #: :type : Story
            story = Story.objects.filter((Q(parts__in=good_parts) | Q(parts__isnull=True)) & Q(finished=False))\
                .order_by('?').first()
            try:
                prev_part = story.parts.get(next__isnull=True)
            except StoryPart.DoesNotExist:
                prev_part = None

            last_part = StoryPart.objects.create(user=user, story=story, prev=prev_part)
            Lock.objects.create(user=user, part=last_part)

            if request.POST:
                return HttpResponse('подозрительное действие')

        if request.POST:
            form = StoryPartForm(request.POST, instance=last_part)
            if form.is_valid():
                last_part = form.save()
                Lock.objects.filter(user=user).delete()
                return redirect(reverse('index'))
        else:
            form = StoryPartForm(instance=last_part)

        context = {
            'story': story,
            'prev_part': prev_part,
            'form': form,
        }

    else:  # if not user.is_authenticated
        context = {}

    return render(request, 'core/index.html', context)
