# coding=utf-8
from django.db import models
# from django.contrib.authentication import get_user_model
from django.core.validators import MinLengthValidator
from django.contrib.auth.models import User

from .managers import BaseManager


# User = get_user_model()


class Story(models.Model):
    finished = models.BooleanField(verbose_name=u'завершена', default=False)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name=u'дата создания')


class StoryPart(models.Model):
    story = models.ForeignKey('Story', related_name='parts', verbose_name=u'история')
    text = models.TextField(max_length=200, verbose_name=u'текст', validators=[MinLengthValidator(200)])
    user = models.ForeignKey(User, verbose_name=u'пользователь', blank=False, null=False)
    prev = models.OneToOneField('self', verbose_name=u'предыдущая часть', related_name='next', blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name=u'дата создания')

    objects = BaseManager()

    def get_next(self):
        return self.objects.get_or_none(prev_id=self.pk)


class Lock(models.Model):
    user = models.OneToOneField(User, verbose_name=u'пользователь')
    part = models.OneToOneField(StoryPart, verbose_name=u'часть истории')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name=u'дата создания')

    class Meta:
        unique_together = ('user', 'part')