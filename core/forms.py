from django import forms
from core.models import StoryPart


class StoryPartForm(forms.ModelForm):
    class Meta:
        model = StoryPart
        widgets = {
            'story': forms.HiddenInput(),

        }
        exclude = ('user', 'prev', 'created_at', )
