# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Lock',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='\u0434\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
            ],
        ),
        migrations.CreateModel(
            name='Story',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('finished', models.BooleanField(default=False, verbose_name='\u0437\u0430\u0432\u0435\u0440\u0448\u0435\u043d\u0430')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='\u0434\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
            ],
        ),
        migrations.CreateModel(
            name='StoryPart',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', models.TextField(max_length=200, verbose_name='\u0442\u0435\u043a\u0441\u0442', validators=[django.core.validators.MinLengthValidator(200)])),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='\u0434\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
                ('prev', models.OneToOneField(related_name='next', null=True, blank=True, to='core.StoryPart', verbose_name='\u043f\u0440\u0435\u0434\u044b\u0434\u0443\u0449\u0430\u044f \u0447\u0430\u0441\u0442\u044c')),
                ('story', models.ForeignKey(related_name='parts', verbose_name='\u0438\u0441\u0442\u043e\u0440\u0438\u044f', to='core.Story')),
                ('user', models.ForeignKey(verbose_name='\u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044c', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='lock',
            name='part',
            field=models.OneToOneField(verbose_name='\u0447\u0430\u0441\u0442\u044c \u0438\u0441\u0442\u043e\u0440\u0438\u0438', to='core.StoryPart'),
        ),
        migrations.AddField(
            model_name='lock',
            name='user',
            field=models.OneToOneField(verbose_name='\u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044c', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterUniqueTogether(
            name='lock',
            unique_together=set([('user', 'part')]),
        ),
    ]
