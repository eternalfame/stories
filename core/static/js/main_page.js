ready(function () {
    var textarea = document.getElementById('id_text');
    addEventListener(textarea, 'input', function () {
        if (this.value.length > 200) {
            this.value = this.value.substring(0, 200);
        }
    });
});
